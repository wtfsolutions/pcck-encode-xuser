![xencode.png](https://bitbucket.org/repo/BnrEBA/images/1402313552-xencode.png)

# XEncode

This little tool creates an encoded version of your xuser and password
combination to use it in your .npmrc.

## How to

- Clone repository to a prefered location on your machine.
- "npm run setup".
- Use it with "xencode encode" from anywhere an your machine.
- On initial use you will be asked to define your xuser id.
- Use option -u to change your xuser id.
- Use option -w to write your changes to ~/.npmrc \_auth=.


## To Dos

- [ ] Investigate why option is not auto generated to help file.
- [ ] Add windows compatibility.


## Changelog

### 0.1.0
- Option to write npmrc file.
- xUser-String toLowercase() since it has influence on the encryption.

### 0.0.4
- Use buffers and character encodings from node.
- Option to save and edit xuser.