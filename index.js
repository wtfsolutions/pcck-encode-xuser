#!/usr/bin/env node --harmony
'use strict';

const program   = require('commander'),
      co        = require('co'),
      prompt    = require('co-prompt'),
      chalk     = require('chalk'),
      pkg       = require('./package.json'),
      usercfg   = require('./config.json'),
      clipboard = require('child_process').spawn('pbcopy'),
      exec      = require('child_process').exec,
      fs        = require("fs")
      ;


function writeUserNpmConfig (key) {
  let command = `npm config set _auth ${key} && cat ~/.npmrc`;

  let execCallback = (error, stdout, stderr) => {
    if (error) console.log("exec error: " + error);
    if (stdout) {
      console.log(chalk.red('npmrc has been written!'));
      console.log(' ');
      console.log(chalk.bold("NPMRC: "));
      console.log(stdout);
    }
    if (stderr) console.log("shell error: " + stderr);
  };
  exec(command, execCallback);
};


function writeConfig (xuser) {
  xuser.xuserid = xuser.xuserid.toLowerCase();
  fs.writeFileSync(__dirname +'/' + 'config.json', JSON.stringify(xuser));
}


let encode = (options)  => {
  let xuser = usercfg;
  let key;

  co(function *() {

    if(xuser.isdefault) {

      xuser.xuserid = yield prompt('setup your xuser id: ');
      xuser.isdefault = false;
      writeConfig(xuser);

    } else if (options.user) {

      xuser.xuserid = yield prompt('change your xuser id: ');
      writeConfig(xuser);

    }

    let password = yield prompt.password('password: ');
    const buf = Buffer.from(xuser.xuserid + ':' + password, 'ascii');
    key = buf.toString('base64');

    console.log(' ');
    console.log(chalk.green('encrypted key: ') + key);

    if (options.write) {
      writeUserNpmConfig(key)
    } else {
      clipboard.stdin.write(key);
      console.log(chalk.green('copied to clipboard!'));
    }

    clipboard.stdin.end();

  });

};

program
  .version(pkg.version)
  .command('encode')
  .option('-u, --user', 'overwirte xuserid')
  .option('-w, --write', 'write encryption key to global npmrc')
  .action(encode);

program.on('--help', function(){
  console.log('    -u, --user \t   overwirte xuserid');
  console.log('    -w, --write    write encryption key to global npmrc');
  console.log('');
});

program.parse(process.argv);

// if program was called with no arguments, show help.
if (program.args.length === 0) program.help();
